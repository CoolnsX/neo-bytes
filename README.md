# Neo-bytes

This is a repository created for Neo-bytes. It contains 1 script and 1 docker file and 1 Readme.. It's just a hello world program in bash and python..

## bash

```sh
chmod +x my_first_script.sh
./my_first_script.sh
```

##python

```sh
chmod +x my_first_script.py
./my_first_script.py
```

##docker

```
docker build ./Dockerfile
```
note: you should be in same directory in which this dockerfile resides
